package com.almworks.entitylocker.escalation;

public interface LockEscalationStrategy {
    boolean isNeedRunEscalation(EntityLockerEscalationApi api);
}
