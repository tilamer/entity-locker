package com.almworks.entitylocker.escalation;

public class NeverStrategy implements LockEscalationStrategy {
    @Override
    public boolean isNeedRunEscalation(EntityLockerEscalationApi api) {
        return false;
    }
}
