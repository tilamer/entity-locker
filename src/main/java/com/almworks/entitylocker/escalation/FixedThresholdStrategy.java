package com.almworks.entitylocker.escalation;

public class FixedThresholdStrategy implements LockEscalationStrategy {
    private final long threshold;

    public FixedThresholdStrategy(long threshold) {
        this.threshold = threshold;
    }

    @Override
    public boolean isNeedRunEscalation(EntityLockerEscalationApi api) {
        return api.getLockedKeysCountByCurrentThread(Thread.currentThread().getId())> threshold;
    }
}
