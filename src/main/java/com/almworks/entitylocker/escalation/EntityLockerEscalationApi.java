package com.almworks.entitylocker.escalation;

public interface EntityLockerEscalationApi {
    long getLockedKeysCountByCurrentThread(long threadId);
}
