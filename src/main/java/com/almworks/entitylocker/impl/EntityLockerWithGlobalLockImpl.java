package com.almworks.entitylocker.impl;

import com.almworks.entitylocker.EntityLockerWithGlobalLock;
import com.almworks.entitylocker.GlobalLocker;
import com.almworks.entitylocker.TimeOutException;
import com.almworks.entitylocker.escalation.LockEscalationStrategy;
import com.almworks.entitylocker.escalation.NeverStrategy;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class EntityLockerWithGlobalLockImpl extends EntityLockerImpl implements EntityLockerWithGlobalLock {
    private ReentrantReadWriteLock globalLock = new ReentrantReadWriteLock(false);
    private LockEscalationStrategy lockEscalationStrategy = new NeverStrategy();
    private volatile ReentrantLock escalationLock = null;
    private volatile long threadId = 0;

    public EntityLockerWithGlobalLockImpl() {
    }

    public EntityLockerWithGlobalLockImpl(LockEscalationStrategy lockEscalationStrategy) {
        this.lockEscalationStrategy = lockEscalationStrategy;
    }

    @Override
    public void lockGlobalAndExecute(Runnable protectedCode) {
        checkEscalationLock();
        globalLock.writeLock().lock();
        try {
            protectedCode.run();
        } finally {
            globalLock.writeLock().unlock();
            tryRemoveEscalationLock();
        }
    }

    @Override
    public void lockAndExecute(Object entityId, Runnable protectedCode) {
        checkEscalationLock();
        globalLock.readLock().lock();
        try {
            tryRunEscalation();
            super.lockAndExecute(entityId, protectedCode);
        } finally {
            globalLock.readLock().unlock();
            tryRemoveEscalationLock();
        }
    }

    @Override
    public void lockAndExecute(Object entityId, Runnable protectedCode, long timeout, TimeUnit unit) throws TimeOutException {
        checkEscalationLock();
        globalLock.readLock().lock();
        try {
            tryRunEscalation();
            super.lockAndExecute(entityId, protectedCode, timeout, unit);
        } finally {
            globalLock.readLock().unlock();
            tryRemoveEscalationLock();
        }
    }

    private void checkEscalationLock() {
        ReentrantLock reentrantLock = this.escalationLock;
        if (reentrantLock!=null) {
            reentrantLock.lock();
            reentrantLock.unlock();
        }
    }

    private void tryRunEscalation() {
        if (escalationLock==null && lockEscalationStrategy.isNeedRunEscalation(this)) {
            runEscalation();
        }
    }

    private void tryRemoveEscalationLock() {
        if (escalationLock!=null && Thread.currentThread().getId()==threadId) {
            ReentrantLock escalationLock = this.escalationLock;
            this.escalationLock = null;
            escalationLock.unlock();
        }
    }

    private synchronized void runEscalation() {
        if (escalationLock==null) {
            ReentrantLock lock = new ReentrantLock(true);
            lock.lock();
            this.threadId = Thread.currentThread().getId();
            this.escalationLock = lock;
        }
    }
}
