package com.almworks.entitylocker.impl;

import com.almworks.entitylocker.DeadlockException;
import com.almworks.entitylocker.EntityLocker;
import com.almworks.entitylocker.TimeOutException;
import com.almworks.entitylocker.escalation.EntityLockerEscalationApi;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class EntityLockerImpl implements EntityLocker, EntityLockerEscalationApi {
    private final int deadlockDetectionTimeout;
    private final TimeUnit unit;

    private Map<Object, ResourceLockWithInterestRate> locksPerResource = new ConcurrentHashMap<>();

    private Map<Object, Long> resourceOwners = new ConcurrentHashMap<>();
    private Map<Long, Object> threadInterest = new ConcurrentHashMap<>();
    private Map<Long, Set<Object>> resourcesPerThread = new ConcurrentHashMap<>();


    public EntityLockerImpl(int deadlockDetectionTimeout, TimeUnit unit) {
        this.deadlockDetectionTimeout = deadlockDetectionTimeout;
        this.unit = unit;
    }

    public EntityLockerImpl() {
        this(100, TimeUnit.MILLISECONDS);
    }

    @Override
    public void lockAndExecute(Object entityId, Runnable protectedCode) {
        lockAndExecute(entityId, protectedCode, this.deadlockDetectionTimeout, this.unit, false);
    }

    @Override
    public void lockAndExecute(Object entityId, Runnable protectedCode, long timeout, TimeUnit unit) throws TimeOutException {
        lockAndExecute(entityId, protectedCode, timeout, unit, true);
    }

    private void lockAndExecute(Object entityId, Runnable protectedCode, long timeout, TimeUnit unit, boolean canThrowTimeoutException) {
        ResourceLockWithInterestRate resourceLockWithInterestRate = getOrCreateLockAndIncrementInterest(entityId);
        long threadId = Thread.currentThread().getId();
        threadInterest.put(threadId, entityId);
        try {
            while (!resourceLockWithInterestRate.getReentrantLock().tryLock(timeout, unit)) {
                if (canThrowTimeoutException) {
                    throw new TimeOutException();
                } else if(isDeadlockHappensAfterLocking(entityId, threadId)) {
                    throw new DeadlockException();
                }
            }
            resourceOwners.put(entityId, threadId);
            attachResourceToThread(entityId, threadId);
            threadInterest.remove(threadId);
            try {
                protectedCode.run();
            } finally {
                resourceOwners.remove(entityId);
                detachResourceFromThread(entityId, resourceLockWithInterestRate, threadId);
                resourceLockWithInterestRate.getReentrantLock().unlock();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            threadInterest.remove(threadId);
            decrementInterest(entityId);
        }
    }

    @Override
    public long getLockedKeysCountByCurrentThread(long threadId) {
        Set<Object> keys = resourcesPerThread.get(threadId);
        return keys==null?0:keys.size();
    }

    public int getLockedEntityCount() {
        return locksPerResource.size();
    }

    private void attachResourceToThread(Object entityId, long threadId) {
        resourcesPerThread.compute(threadId, (key, value) -> {
            if (value==null) {
                value = new HashSet<>();
            }
            value.add(entityId);
            return value;
        });
    }

    private void detachResourceFromThread(Object entityId, ResourceLockWithInterestRate resourceLockWithInterestRate, long threadId) {
        resourcesPerThread.compute(threadId, (key, value) -> {
            if (value==null) {
                return null;
            } else {
                value.remove(entityId);
                if (value.size()==0) {
                    return null;
                } else {
                    return value;
                }
            }
        });
    }


    private boolean isDeadlockHappensAfterLocking(Object entityId, Long threadId) {
        Long ownerThreadId = resourceOwners.get(entityId);
        if (ownerThreadId!=null) {
            if (ownerThreadId.equals(threadId)) {
                return true;
            } else {
                Object ownerThreadInterest = threadInterest.get(ownerThreadId);
                if (ownerThreadInterest!=null) {
                    return isDeadlockHappensAfterLocking(ownerThreadInterest, threadId);
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private ResourceLockWithInterestRate getOrCreateLockAndIncrementInterest(Object entityId) {
        return locksPerResource.compute(entityId, (key, value) -> {
            if (value == null) {
                value = new ResourceLockWithInterestRate();
            }
            return value.incrementInterest();
        });
    }

    private void decrementInterest(Object entityId) {
        locksPerResource.computeIfPresent(entityId, (key, value) -> {
            value.decrementInterest();
            if (value.getInterest() == 0) {
                return null;
            } else {
                return value;
            }
        });
    }

    private static class ResourceLockWithInterestRate {
        private ReentrantLock reentrantLock = new ReentrantLock(false);
        private long interest = 0;

        ReentrantLock getReentrantLock() {
            return reentrantLock;
        }

        long getInterest() {
            return interest;
        }

        ResourceLockWithInterestRate incrementInterest() {
            interest++;
            return this;
        }

        void decrementInterest() {
            interest--;
        }
    }
}
