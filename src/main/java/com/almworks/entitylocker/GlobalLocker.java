package com.almworks.entitylocker;

public interface GlobalLocker {
    void lockGlobalAndExecute(Runnable protectedCode);
}
