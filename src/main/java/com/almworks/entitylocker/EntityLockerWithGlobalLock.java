package com.almworks.entitylocker;

public interface EntityLockerWithGlobalLock extends EntityLocker, GlobalLocker {
}
