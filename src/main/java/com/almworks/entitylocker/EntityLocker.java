package com.almworks.entitylocker;

import java.util.concurrent.TimeUnit;

public interface EntityLocker {
    void lockAndExecute(Object entityId, Runnable protectedCode);
    void lockAndExecute(Object entityId, Runnable protectedCode, long timeout, TimeUnit unit) throws TimeOutException;
}
