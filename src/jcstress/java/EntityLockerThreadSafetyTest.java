package com.almworks.entitylocker.impl;

import org.openjdk.jcstress.annotations.*;
import org.openjdk.jcstress.infra.results.IIIII_Result;

import java.util.concurrent.atomic.AtomicInteger;

import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE;
import static org.openjdk.jcstress.annotations.Expect.FORBIDDEN;

public class EntityLockerThreadSafetyTest {
    @State
    public static class EntityLockerState {
        final EntityLockerImpl entityLocker = new EntityLockerImpl();
        final AtomicInteger atomicInteger = new AtomicInteger(0);
        final AtomicInteger atomicInteger2 = new AtomicInteger(0);
        final AtomicInteger atomicInteger3 = new AtomicInteger(0);
        final AtomicInteger atomicInteger4 = new AtomicInteger(0);
        final AtomicInteger atomicInteger5 = new AtomicInteger(0);
    }

    @JCStressTest
    @Description("Test cuncurent execition protected code for single resource")
    @Outcome(id = "0, 1, 0, 1, 0", expect = ACCEPTABLE, desc = "return 0-locks count after test,1 0 1 0 - states of thread")
    @Outcome(expect = FORBIDDEN, desc = "protected code was running in parallel mode")
    public static class EntityLockerDeadlockTest {

        public static final String ID_1 = "id1";
        public static final String ID_2 = "id2";


        @Actor
        public void actor1(EntityLockerState state, IIIII_Result result) {
            state.entityLocker.lockAndExecute(ID_1, () -> {
                state.atomicInteger2.set(state.atomicInteger.incrementAndGet());
                Thread.yield();
                state.atomicInteger3.set(state.atomicInteger.decrementAndGet());
            });
        }

        @Actor
        public void actor2(EntityLockerState state, IIIII_Result result) {
            state.entityLocker.lockAndExecute(ID_1, () -> {
                state.atomicInteger4.set(state.atomicInteger.incrementAndGet());
                Thread.yield();
                state.atomicInteger5.set(state.atomicInteger.decrementAndGet());
            });
        }

        @Arbiter
        public void arbiter(EntityLockerState state, IIIII_Result r) {
            r.r1 = state.entityLocker.getLockedEntityCount();
            r.r2 = state.atomicInteger2.get();
            r.r3 = state.atomicInteger3.get();
            r.r4 = state.atomicInteger4.get();
            r.r5 = state.atomicInteger5.get();
        }
    }
}
