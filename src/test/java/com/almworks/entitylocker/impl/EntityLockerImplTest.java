package com.almworks.entitylocker.impl;

import com.almworks.entitylocker.DeadlockException;
import com.almworks.entitylocker.TestUtil;
import com.almworks.entitylocker.TimeOutException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

class EntityLockerImplTest {

    public static final String ENTITY_ID_1 = "entityId1";
    public static final String ENTITY_ID_2 = "entityId2";

    @Test
    public void whenTwoThreadsTryLockOneIdThenOnlyOneThreadShouldRunProtectedCode() {
        ReentrantLock reentrantLock = new ReentrantLock(true);
        reentrantLock.lock();
        EntityLockerImpl entityLocker = new EntityLockerImpl();
        Thread t1 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_1, TestUtil.wrap(reentrantLock::lock));
        });
        Thread t2 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_1, TestUtil.wrap(reentrantLock::lock));
        });

        TestUtil.waitUntilThreadBlocks(t1);
        TestUtil.waitUntilThreadBlocks(t2);
        Assertions.assertEquals(1, entityLocker.getLockedEntityCount(), "current locked keys count");
        Assertions.assertTrue(TestUtil.inProtectedBlock(t1) && !TestUtil.inProtectedBlock(t2) || TestUtil.inProtectedBlock(t2) && !TestUtil.inProtectedBlock(t1), "at most one thread paused in protected code");
        reentrantLock.unlock();
    }

    @Test
    public void whenTwoThreadsLockDifferentIdsThenTheyShouldRunProtectedCodeTogether() {
        ReentrantLock reentrantLock = new ReentrantLock(true);
        reentrantLock.lock();
        EntityLockerImpl entityLocker = new EntityLockerImpl();
        Thread t1 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_1, TestUtil.wrap(reentrantLock::lock));
        });
        Thread t2 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_2, TestUtil.wrap(reentrantLock::lock));
        });

        TestUtil.waitUntilThreadBlocks(t1);
        TestUtil.waitUntilThreadBlocks(t2);
        Assertions.assertEquals(2, entityLocker.getLockedEntityCount(), "current locked keys count");
        Assertions.assertTrue(TestUtil.inProtectedBlock(t1) && TestUtil.inProtectedBlock(t2), "all threads paused in protected code");
        reentrantLock.unlock();
    }

    @Test
    public void whenEveryThreadCompleteProtectedCodeThenLockerShouldHaveNoOneLockedId() throws InterruptedException {
        AtomicInteger atomicInteger = new AtomicInteger();
        EntityLockerImpl entityLocker = new EntityLockerImpl();
        Thread t1 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_1, atomicInteger::incrementAndGet);
            entityLocker.lockAndExecute(ENTITY_ID_2, atomicInteger::incrementAndGet);
        });
        Thread t2 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_2, atomicInteger::incrementAndGet);
            entityLocker.lockAndExecute(ENTITY_ID_1, atomicInteger::incrementAndGet);
        });

        t1.join();
        t2.join();
        Assertions.assertEquals(4, atomicInteger.get(), "protected code execution count");
        Assertions.assertEquals(0, entityLocker.getLockedEntityCount(), "current locked keys count");
    }

    @Test
    public void whenThreadLockIdSecondTimeUnderProtectedCodeThenSecondProtectedCodeShouldRun() {
        AtomicInteger atomicInteger = new AtomicInteger();
        EntityLockerImpl entityLocker = new EntityLockerImpl();
        entityLocker.lockAndExecute(ENTITY_ID_1, () -> {
            entityLocker.lockAndExecute(ENTITY_ID_1, () -> {
                atomicInteger.incrementAndGet();
            });
        });
        Assertions.assertEquals(0, entityLocker.getLockedEntityCount(), "current locked keys count");
        Assertions.assertEquals(1, atomicInteger.get(), "protected code execution count");
    }

    @Test
    public void whenThreadsTryLockAlreadyLockedIdWithTimeoutThenTimeoutExceptionShouldBeThrowenAfterCertainTime() throws InterruptedException {
        AtomicInteger atomicInteger = new AtomicInteger();
        ReentrantLock reentrantLock = new ReentrantLock(true);
        reentrantLock.lock();
        EntityLockerImpl entityLocker = new EntityLockerImpl();
        Thread t1 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_1, TestUtil.wrap(() -> {
                atomicInteger.incrementAndGet();
                reentrantLock.lock();
            }));
        });
        TestUtil.waitUntilThreadBlocks(t1);
        Assertions.assertThrows(TimeOutException.class, () -> {
            entityLocker.lockAndExecute(ENTITY_ID_1, TestUtil.wrap(() -> {
                        atomicInteger.incrementAndGet();
                        reentrantLock.lock();
                    }), 10, TimeUnit.MILLISECONDS
            );
        }, "TimeOutException will be thrown if the lock is not acquired within a certain time");

        reentrantLock.unlock();
        t1.join();
        Assertions.assertEquals(0, entityLocker.getLockedEntityCount(), "current locked keys count");
        Assertions.assertEquals(1, atomicInteger.get(), "protected code execution count");
    }

    @Test
    public void whenThreadsTryLockAlreadyLockedIdFromProtectedCodeThenDeadlockExceptionShouldThrown() throws InterruptedException {
        EntityLockerImpl entityLocker = new EntityLockerImpl();
        ReentrantLock reentrantLock1 = new ReentrantLock(true);
        ReentrantLock reentrantLock2 = new ReentrantLock(true);
        AtomicBoolean t1Exception = new AtomicBoolean(false);
        AtomicBoolean mainException = new AtomicBoolean(false);
        reentrantLock1.lock();
        Thread t1 = TestUtil.runInThread(() -> {
            reentrantLock2.lock();
            entityLocker.lockAndExecute(ENTITY_ID_1, () -> {
                reentrantLock1.lock();
                reentrantLock2.unlock();
                try {
                    entityLocker.lockAndExecute(ENTITY_ID_2, () -> {
                    });
                } catch (DeadlockException e) {
                    t1Exception.set(true);
                }
            });
        });
        TestUtil.waitUntilThreadBlocks(t1);
        entityLocker.lockAndExecute(ENTITY_ID_2, () -> {
            reentrantLock1.unlock();
            reentrantLock2.lock();
            TestUtil.waitUntilThreadBlocks(t1);
            try {
                entityLocker.lockAndExecute(ENTITY_ID_1, () -> {
                });
            } catch (DeadlockException e) {
                mainException.set(true);
            }

        });

        t1.join();
        Assertions.assertTrue(mainException.get() && !t1Exception.get() || !mainException.get() && t1Exception.get(), "any thread involved in deadlock should throw DeadlockException ");

        Assertions.assertEquals(0, entityLocker.getLockedEntityCount(), "current locked keys count");
    }

}