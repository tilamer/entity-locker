package com.almworks.entitylocker.impl;

import com.almworks.entitylocker.TestUtil;
import com.almworks.entitylocker.TimeOutException;
import com.almworks.entitylocker.escalation.FixedThresholdStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

class EntityLockerWithGlobalLockImplTest {
    public static final String ENTITY_ID_1 = "entityId1";
    public static final String ENTITY_ID_2 = "entityId2";
    public static final String ENTITY_ID_3 = "entityId3";

    @Test
    public void whenThreadAcquireGlobalLockThenNoOneThreadShouldRunProtectedCode() {
        ReentrantLock reentrantLock = new ReentrantLock(true);
        reentrantLock.lock();
        EntityLockerWithGlobalLockImpl entityLocker = new EntityLockerWithGlobalLockImpl();
        Thread t1 = TestUtil.runInThread(() -> {
            entityLocker.lockGlobalAndExecute(TestUtil.wrap(reentrantLock::lock));
        });
        TestUtil.waitUntilThreadBlocks(t1);
        Thread t2 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_1, TestUtil.wrap(reentrantLock::lock));
        });
        TestUtil.waitUntilThreadBlocks(t2);
        Assertions.assertEquals(0, entityLocker.getLockedEntityCount(), "current locked keys count");
        Assertions.assertTrue(TestUtil.inProtectedBlock(t1) && !TestUtil.inProtectedBlock(t2), "only first thread should execute protected code under global lock");
        reentrantLock.unlock();
    }

    @Test
    public void whenTwoThreadsAcquireGlobalLockThenOnlyOneShouldRunProtectedCode() {
        ReentrantLock reentrantLock = new ReentrantLock(true);
        reentrantLock.lock();
        EntityLockerWithGlobalLockImpl entityLocker = new EntityLockerWithGlobalLockImpl();
        Thread t1 = TestUtil.runInThread(() -> {
            entityLocker.lockGlobalAndExecute(TestUtil.wrap(reentrantLock::lock));
        });
        Thread t2 = TestUtil.runInThread(() -> {
            entityLocker.lockGlobalAndExecute(TestUtil.wrap(reentrantLock::lock));
        });

        TestUtil.waitUntilThreadBlocks(t1);
        TestUtil.waitUntilThreadBlocks(t2);
        Assertions.assertEquals(0, entityLocker.getLockedEntityCount(), "current locked keys count");
        Assertions.assertTrue(TestUtil.inProtectedBlock(t1) && !TestUtil.inProtectedBlock(t2) || !TestUtil.inProtectedBlock(t1) && TestUtil.inProtectedBlock(t2), "only one thread should execute protected code");
        reentrantLock.unlock();
    }


    @Test
    public void whenAnyThreadAcquireResourceLockThenNoOneThreadShouldRunProtectedCodeUnderGlobalLock() {
        ReentrantLock reentrantLock = new ReentrantLock(true);
        reentrantLock.lock();
        EntityLockerWithGlobalLockImpl entityLocker = new EntityLockerWithGlobalLockImpl();
        Thread t1 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_1, TestUtil.wrap(reentrantLock::lock));
        });
        TestUtil.waitUntilThreadBlocks(t1);
        Thread t2 = TestUtil.runInThread(() -> {
            entityLocker.lockGlobalAndExecute(TestUtil.wrap(reentrantLock::lock));
        });
        TestUtil.waitUntilThreadBlocks(t2);
        Assertions.assertEquals(1, entityLocker.getLockedEntityCount(), "current locked keys count");
        Assertions.assertTrue(TestUtil.inProtectedBlock(t1) && !TestUtil.inProtectedBlock(t2), "only first thread should execute protected code under resource lock");
        reentrantLock.unlock();
    }

    @Test
    public void whenAnyThreadEscaleteGlobalLockThenNoOneNewThreadShouldRunProtectedCode() {
        ReentrantLock reentrantLock = new ReentrantLock(true);
        reentrantLock.lock();
        EntityLockerWithGlobalLockImpl entityLocker = new EntityLockerWithGlobalLockImpl(new FixedThresholdStrategy(0));
        Thread t1 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_1, () -> {
                entityLocker.lockAndExecute(ENTITY_ID_2, TestUtil.wrap(reentrantLock::lock));
            });
        });
        TestUtil.waitUntilThreadBlocks(t1);
        Thread t2 = TestUtil.runInThread(() -> {
            entityLocker.lockAndExecute(ENTITY_ID_3, TestUtil.wrap(reentrantLock::lock));
        });
        TestUtil.waitUntilThreadBlocks(t2);
        Thread t3 = TestUtil.runInThread(() -> {
            entityLocker.lockGlobalAndExecute(TestUtil.wrap(reentrantLock::lock));
        });
        TestUtil.waitUntilThreadBlocks(t3);
        Assertions.assertEquals(2, entityLocker.getLockedEntityCount(), "current locked keys count");
        Assertions.assertTrue(TestUtil.inProtectedBlock(t1) && !TestUtil.inProtectedBlock(t2) && !TestUtil.inProtectedBlock(t3), "only first thread should execute protected code under resource lock");
        reentrantLock.unlock();
    }
}