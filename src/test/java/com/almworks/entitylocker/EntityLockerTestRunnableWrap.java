package com.almworks.entitylocker;

public class EntityLockerTestRunnableWrap implements Runnable {
    private final Runnable runnable;

    public EntityLockerTestRunnableWrap(Runnable runnable) {
        this.runnable = runnable;
    }

    @Override
    public void run() {
        runnable.run();
    }
}
