package com.almworks.entitylocker;

public class TestUtil {


    public static Runnable wrap(Runnable value) {
        return new EntityLockerTestRunnableWrap(value);
    }

    public static void waitUntilThreadBlocks(Thread thread) {
        while (!isBlocked(thread)) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isNotBlocked(Thread thread) {
        return thread.getState()==Thread.State.NEW || thread.getState()==Thread.State.RUNNABLE || thread.getState()==Thread.State.TERMINATED;
    }

    public static boolean isBlocked(Thread thread) {
        return thread.getState()==Thread.State.BLOCKED || thread.getState()==Thread.State.WAITING || thread.getState()==Thread.State.TIMED_WAITING;
    }

    public static boolean inProtectedBlock(Thread thread) {
        for (StackTraceElement stackTraceElement : thread.getStackTrace()) {
            if (stackTraceElement.getClassName().equals(EntityLockerTestRunnableWrap.class.getName())) {
                return true;
            }
        }
        return false;
    }

    public static Thread runInThread(Runnable runnable) {
        Thread t = new Thread(runnable);
        t.setDaemon(true);
        t.start();
        return t;
    }


}
